| Solutions                                                                                         | Prix | Stockage | + Stockage           | Commentaire                                                                    |
|---------------------------------------------------------------------------------------------------|------|----------|----------------------|--------------------------------------------------------------------------------|
| [Google Suite](https://workspace.google.com/pricing.html) [+](https://one.google.com/about/plans) | 1134 | 2TB      | 5TB +150 / an        | -5€ de mail en utilisant gmail                                                 |
| [Own Cloud](https://owncloud.com/pricing/)                                                        | 2340 | 4TB      | +200GB / user (156€) |                                                                                |
| [AWS FSX](https://aws.amazon.com/fsx/windows/pricing/?nc=sn&loc=3)                                | 435  | 500GB    | 0.076$/ GB/ mois     | Pay as you go. Pas d'interface web (windows only). Potentiel prix de transfert |
| [Next Cloud](https://nextcloud.com/fr)                                                            | ~560 | 500GB    | $0.0928 / GB / mois  | Suite nextcloud, besoin potentiel d'un admin. transfert 0.09 / DB              |


