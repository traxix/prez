#+Title: Neurochain
#+Author: trax Omar Givernaud

#+OPTIONS: toc:1
#+REVEAL_THEME: night
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_TRANS: cube


* La BlockChain 

+ Un livre de compte. 
+ Système distribué. 
+ Temps réel.
+ Des monnaies évaluées à plus de 200 $Md https://coinmarketcap.com/
+ La consommation de 4 centrales nucléaires.

* Neurochain 

** 

#+REVEAL_HTML: <iframe width="560" height="315" src="https://www.youtube.com/embed/yb5fwC7C_3s" frameborder="0" allow="encrypted-media; picture-in-picture" allowfullscreen></iframe>


** 

+ Un nouveau consensus.
+ Rapide.
+ Décentralisée.


* Lead Tech 

** Tech

  + Outils.
  + Architecture.
  + Écriture/revue de code. 
  + QA.
  + Déploiment.
  + Maintenance/Support.

** Lead

  + Recrutement.
  + Organisation.
  + Arbitre.
  + Responsabilités.

* Parcours/Conseils 

+ Club*Nix
+ 6 entreprises depuis 2011.

* Liens

+ https://www.neurochaintech.io/
+ https://gitlab.com/neurochaintech/core/
+ https://web.telegram.org/#/im?p=@neurochain_official


